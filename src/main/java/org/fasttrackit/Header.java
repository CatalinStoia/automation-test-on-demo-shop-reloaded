package org.fasttrackit;

public class Header {

    public static final String URL = "https://fasttrackit-test.netlify.app";
    private String url = URL;
    public void clickOnTheLoginButton() {

        System.out.println("Click on The login button.");
    }
    public String getGreetingsMessage(){
        return "Hello guest!";
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Whishlist button.");
        url = URL + "/#/wishlist";
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the shopping bag icon.");
        url = URL;
    }

    public String getUrl() {
        return url;
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the cart icon.");
        url = URL + "/#/cart";
    }
}

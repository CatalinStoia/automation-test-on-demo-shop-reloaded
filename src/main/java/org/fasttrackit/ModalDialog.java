package org.fasttrackit;

public class ModalDialog {

    public void typeInUsername(String username) {
        System.out.println("Clicking on the Username field.");
        System.out.println("Type in " + username);
    }

    public void typeInPassword(String password) {
        System.out.println("Click on the password field.");
        System.out.println("Type in " + password);
    }

    public void clickOnTheLoginButton() {
        System.out.println("Click on the login button.");
    }
}

package org.fasttrackit;

public class DemoShop {
    public static void main(String[] args) {

        System.out.println("== Demo Shop ==");
        System.out.println("1. User can log in with valid credentials.");
        HomePage homePage = new HomePage(); // create homePage object
        homePage.openHomePage();
        Header header = new Header(); // create Header object
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();// create login modal object
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi Dino is displayed in the header: " + isLogged);
        System.out.println("Greetings message is: " + greetingsMessage);

        System.out.println("--------------------------------------");
        System.out.println("2. User can add product to cart from product cards.");
        homePage.openHomePage();
        ProductCards cards = new ProductCards();
        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
        System.out.println("Product is: " + awesomeGraniteChips);
        awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println("--------------------------------------");
        System.out.println("3.User can navigate to Homepage from Wishlist page.");
        homePage.openHomePage();
        header.clickOnTheWishlistIcon();
        String url = header.getUrl();
        System.out.println("Expected to be on the wishlist page." + url);
        header.clickOnTheShoppingBagIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Homepage." + url);

        System.out.println("--------------------------------------");
        System.out.println("4.User can navigate to Homepage from Cart page.");
        homePage.openHomePage();
        header.clickOnTheCartIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Cart page." + url);
        header.clickOnTheShoppingBagIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Homepage." + url);

    }

}
//- User can navigate to Homepage from Wishlist page
//1. Open Homepage
//2. Click on the "Favourites" Icon
//3. Click on the "Shopping bag" icon
// Expected results: Home Page is loaded

//----------------------------------
// - User can add product to cart from product cards
// 1. Open Home Page
// 2. Click on the "Awesome Granite Chips" cart icon
// 3. Expected results: Mini Cart icon shows one product in cart



//----------------------------------
// 1. Open page.
// 2. Click on the Login Button.
// 3. Click on the Username Field.
// 4. Type in dino.
// 5. Click on the Password Field.
// 6. Type in choochoo.
// 7. Click on the login button.
// Expected: Hi dino is displayed in the header.